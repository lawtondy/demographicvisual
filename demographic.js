$(function () {

	var races = [4903, 1801, 17, 2028, 92, 756, 587, 2669, 24517];
	var majors = [130,22,25,2,87,19,71,97,8,56,5,14,52,81,87,84,9,293,7,415,120,135,29,95,6,5,1,220,3,78,261,22,13,37,14,6,25,1,14,46,487,33,128,51,3,10,233,113,6,40,31,39,74,7,54,8,178,51,18,209,68,4,3,1,17,239,4,5,5,4,4,5,1,50,50,193,260,46,4,6,132,7,1,6,1,1,24,2,46,2,300,30,138,1,59,60,5,1,51,11,3,3,27,1,1,394,518,63,5,5,7,113,56,10,129,2,151,92,1,45,25,58];
	var clicked_race;
	var clicked_major;
	var text_race;
	var text_major;
	var total_student_majors = 8024;
	var total_student_races = 37370; 

    
    Highcharts.setOptions({
        chart: {
            style: {
                fontFamily: 'Raleway'
            }
        }  
    });
 
    
    $('#container_one').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: '65.6% of the University of Michigan is White'
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false,
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                    style: {
	                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: "Proportion",
	            colorByPoint: true,
	            data: [{
	                name: "Selected Race",
	                y: 65.6
	            }, {
	                name: "Other",
	                y: 100-65.6,
	                sliced: true,
	                selected: true
	            }]
	        }],
	        credits: {
      			enabled: false
  			},
  			exporting: {
            	enabled: false
        	}
	    });

	$('#container_two').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: '3.3% of the University of Michigan majors in Computer Science'
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false,
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                    style: {
	                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: "Proportion",
	            colorByPoint: true,
	            data: [{
	                name: "Selected Major",
	                y: 3.3
	            }, {
	                name: "Other",
	                y: 100-3.3,
	                sliced: true,
	                selected: true
	            }]
	        }],
	        credits: {
      			enabled: false
  			},
  			exporting: {
            	enabled: false
        	}
	    });

	$(".race").click(function() {
	    clicked_race = $(this).attr("id");
	    text_race = $(this).html();
	    document.getElementById("race_button").innerHTML = text_race + "  <span class='caret'></span>";
        
	});

	$(".major").click(function() {
	    clicked_major = $(this).attr("id");
	    text_major = $(this).html();
	    document.getElementById("major_button").innerHTML = text_major + "  <span class='caret'></span>";
	});

	$(".go").click(function() {
		// Have the graphs repopulate with the clicked races and majors.
		if(clicked_major == -1){
			clicked_major = Math.round(Math.random() * 121); // index
			text_major = $(".major")[clicked_major + 1].text;
		}


		// Update for chart 1
		var chart_one = $('#container_one').highcharts();
		var race_percentage = Math.round(races[clicked_race] / total_student_races * 100 * 10) / 10;
		var other_race_percentage = 100 - race_percentage;
		var chart_one_text = race_percentage + "% of the University of Michigan is " + text_race;

        chart_one.series[0].setData([race_percentage, other_race_percentage]);
        chart_one.setTitle({text: chart_one_text});

        // Update for chart 2
        var chart_two = $('#container_two').highcharts();
        var major_percentage = Math.round(majors[clicked_major] / total_student_majors * 100 * 10) / 10;
        var other_major_percentage = 100 - major_percentage;
        var chart_two_text;

        if(major_percentage < 0.1) {
			chart_two_text = "Less than 0.1% of the University of Michigan majors in " + text_major;
        }
        else{
        	chart_two_text = major_percentage + "% of the University of Michigan majors in " + text_major;
        }
        

        chart_two.series[0].setData([major_percentage, other_major_percentage]);
        chart_two.setTitle({text: chart_two_text});
        

        // Repopulate the text at the bottom page for the comparison.
        var bottom_text;
        bottom_text = "You are " + Math.round(race_percentage / major_percentage * 10) / 10 + " times more likely to meet " + text_race + " students than " + text_major + " majors at the University of Michigan.";
        document.getElementById("text_change").innerHTML = bottom_text;

	});

});

    




